#!/usr/local/bin/bash

display_usage() {
	echo "#############################################" 
	echo "#                                           #" 
	echo "#    Automated Service Scanner ( ass )      #" 
	echo "#                                           #" 
	echo "#  Will scan all TCP & UDP ports using      #" 
	echo "#  unicornscan, followed by a targeted      #" 
	echo "#  service scan using nmap outputting all   #" 
	echo "#  formats by IP and TCP/UDP                #" 
	echo "#                                           #" 
	echo "#  Options:                                 #" 
	echo "#                                           #" 
	echo "# -i int  interface (e.g. eth0, re0, tap0)  #" 
	echo "#                                           #" 
	echo "#    Scan all ports from ips in targets.txt #" 
	echo "#                                           #" 
	echo "#                                           #" 
	echo "# -s int  interface (e.g. eth0, re0, tap0)  #" 
	echo "#                                           #" 
	echo "#    Automated Search and Destroy           #" 
	echo "#    use arpscan to find network hosts      #" 
	echo "#                                           #" 
	echo "#                                           #" 
	echo "# -h help - display this menu               #" 
	echo "#                                           #" 
	echo "#      Usage: ./ass.sh interface            #" 
	echo "#                                           #" 
	echo "#                                           #" 
	echo "#  NOTE:Requires unicornscan nmap arp-scan  #" 
	echo "#                                           #" 
	echo "#############################################" 
}
	# if less than two arguments supplied, display usage
	if [  $# -le 1 ]
	then
		display_usage
		exit 1
	fi
while getopts ":s:i:h" opt;do
	case $opt in
		s) arg2="${OPTARG}"
			echo "  	  Automated Service Scanner ( ass )      	" 
			echo '                                                         c=====e'
			echo '                                                            H'
			echo '   ____________                                         _,,_H__'
			echo '  (__((__((___()                                       //|     |'
			echo ' (__((__((___()()_____________________________________// |ACME |'
			echo '(__((__((___()()()-------------------------------------  |     |'
			echo '(__((__((_()()()()()-----------------------------------  |_____|'
			echo -e "\n"
			arp-scan -I $arg2 -l| grep -oE '((1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])\.){3}(1?[0-9][0-9]?|2[0-4][0-9]|25[0-5])' > targets.txt

			echo -e "# Automated Search and Destroy Activated!"
			echo -e "# networked devices on $arg2 have been added to targets.txt"
			for i in `cat targets.txt`;do
				echo "# $i <--target";done
				for ip in `cat targets.txt`;do
					echo -e "# Scanning all TCP ports on target $ip on interface: $arg2"
					unicornscan  -i $arg2 -R3 -mT -r 2000  $ip/32:a -R 3 |tee $ip-tcp.txt
					echo -e '# Processing '$ip'-tcp.txt '
					cat $ip-tcp.txt | grep "Open"|  cut -d"[" -f2| cut -d"]" -f1| sort -g| uniq|sed '$!s/$/,/'| tr "\n" " "| sed 's/ //g'   > $ip-ports.tcp.txt
					echo -e '# '$ip'-ports.tcp.txt processed '
					cat $ip-ports.tcp.txt
					echo -e ''
					echo -e '# Performing targeted TCP service scan on target' $ip 
					nmap -A -T4 $ip -oA $ip-nmap-tcp -e $arg2 -p $(cat $ip-ports.tcp.txt)
					echo -e "#" $ip"-TCP done"
					echo -e "# Scanning all UDP ports on target "$ip
					unicornscan -i $arg2 -mU -R3 -r 2000 $ip/32:a | tee $ip-udp.txt
					echo -e '# Processing '$ip'-udp.txt'
					cat $ip-udp.txt | grep "Open"|  cut -d"[" -f2| cut -d"]" -f1| sort -g| uniq|sed '$!s/$/,/'| tr "\n" " "| sed 's/ //g'   > $ip-ports.udp.txt
					echo -e '# '$ip'-ports.udp.txt Processed'
					cat $ip-ports.udp.txt
					echo -e "# Performing targeted UDP service scan on target "$ip
					nmap -sU -sV -v $ip -oA $ip-nmap-udp -e $arg2 -p `cat $ip-ports.udp.txt`
					echo -e "#" $ip"-UDP done";done
					;;
				i) arg="${OPTARG}"
					echo -e "\n\n\n  	  Automated Service Scanner ( ass )      	" 
					echo '                                                         c=====e'
					echo '                                                            H'
					echo '   ____________                                         _,,_H__'
					echo '  (__((__((___()                                       //|     |'
					echo ' (__((__((___()()_____________________________________// |ACME |'
					echo '(__((__((___()()()-------------------------------------  |     |'
					echo '(__((__((_()()()()()-----------------------------------  |_____|'
					echo -e "\n"
					if [ -s targets.txt ];then echo "# Targets available in targets.txt";else echo -e "# No targets in targets.txt\n# Use -s to auto search & scan network\n"; display_usage;fi

					for ip in `cat targets.txt`;do
						echo -e "# Interface selected $arg"
						echo -e "# Scanning all TCP ports on target $ip on interface: $arg"
						unicornscan  -i $arg -R3 -mT -r 2000  $ip/32:a -R 3 |tee $ip-tcp.txt
						echo -e '# Processing '$ip'-tcp.txt '
						cat $ip-tcp.txt | grep "Open"|  cut -d"[" -f2| cut -d"]" -f1| sort -g| uniq|sed '$!s/$/,/'| tr "\n" " "| sed 's/ //g'   > $ip-ports.tcp.txt
						echo -e '# '$ip'-ports.tcp.txt processed '
						echo "# `cat $ip-ports.tcp.txt` <-- TCP ports found"
						echo -e '# Performing targeted TCP service scan on target' $ip
						nmap -A -T4 $ip -oA $ip-nmap-tcp -e $arg -p $(cat $ip-ports.tcp.txt)
						echo -e "#" $ip"-TCP done"
						echo -e "# Scanning all UDP ports on target "$ip
						unicornscan -i $arg -mU -R3 -r 2000 $ip/32:a | tee $ip-udp.txt
						echo -e '# Processing '$ip'-udp.txt' 
						cat $ip-udp.txt | grep "Open"|  cut -d"[" -f2| cut -d"]" -f1| sort -g| uniq|sed '$!s/$/,/'| tr "\n" " "| sed 's/ //g'   > $ip-ports.udp.txt
						echo -e '# '$ip'-ports.udp.txt Processed'
						cat $ip-ports.udp.txt
						echo -e "# Performing targeted UDP service scan on target "$ip
						nmap -sU -sV -v $ip -oA $ip-nmap-udp -e $arg -p `cat $ip-ports.udp.txt`
						echo -e "#" $ip"-UDP done";done
						;;
					h) display_usage >&2
						;;
				esac;done
