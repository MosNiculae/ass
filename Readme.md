```
  	  Automated Service Scanner ( ass )      	
                                                         c=====e
                                                            H
   ____________                                         _,,_H__
  (__((__((___()                                       //|     |
 (__((__((___()()_____________________________________// |ACME |
(__((__((___()()()-------------------------------------  |     |
(__((__((_()()()()()-----------------------------------  |_____|

#############################################
#                                           #
#    Automated Service Scanner ( ass )      #
#                          by makaveli      #
#                                           #
#                                           #
#  Will scan all TCP & UDP ports using      #
#  unicornscan, followed by a targeted      #
#  service scan using nmap outputting all   #
#  formats by IP and TCP/UDP                #
#                                           #
#  Options:                                 #
#                                           #
# -i int  interface (e.g. eth0, re0, tap0)  #
#                                           #
#    Scan all ports from ips in targets.txt #
#                                           #
#                                           #
# -s int  interface (e.g. eth0, re0, tap0)  #
#                                           #
#    Automated Search and Destroy           #
#    use arpscan to find network hosts      #
#                                           #
#                                           #
# -h help - display this menu               #
#                                           #
#       Usage: ./ass -i interface           #
#                                           #
#                                           #
#  NOTE:Requires unicornscan nmap arp-scan  #
#                                           #
#############################################
```
